#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
if [ -f /etc/bash_completiton ]; then
	. /etc/bash_completiton
fi

#### PS1 ####
PS1="\W \$ "

#### Exports ####
export EDITOR=vim
export HISTCONTROL=ignoreboth

#### Aliases ####

alias config='/usr/bin/git --git-dir=/home/luka/Dotfiles/ --work-tree=/home/luka'
alias ls='ls -Al --color=auto'
alias ..='cd ..'
alias rm='rm -i'
alias mv='mv -i'
